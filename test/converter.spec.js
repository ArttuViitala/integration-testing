// TDD - unit testing

const expect = require("chai").expect;
const converter = require("../src/converter");

describe("Color Code Converter", () => {
    describe("RGB to Hex conversions", () => {
        it("converts the basic colors", () => {
            const redHex = converter.rgbToHex(255, 0, 0); // #ff0000
            expect(redHex).to.equal("#ff0000"); // red hex value

            const blueHex = converter.rgbToHex(0, 0, 255); // #0000ff
            expect(blueHex).to.equal("#0000ff"); // blue hex value

            const greenHex = converter.rgbToHex(0, 255, 0); // #00ff00
            expect(greenHex).to.equal("#00ff00"); // green hex value
        });
    });
    describe("Hex to RGB conversion", () => {
        it("converts the basic colors", () => {
            const redRgb = converter.hexTorgb("#ff0000"); // 255 0 0
            expect(redRgb).to.equal(255, 0, 0)
            const greenRgb = converter.hexTorgb("#00ff00"); // 0 255 0
            expect(greenRgb).to.equal(0, 255, 0)
            const blueRgb = converter.hexTorgb("#0000ff"); // 0 0 255
            expect(blueRgb).to.equal(0, 0, 255)
        });
    });
});